var mongoose=require("mongoose");
const dotenv = require("dotenv")
dotenv.config({path: "./config.env"})
var mongoURI=process.env.LOCAL_MONGO;
var schema=mongoose.Schema;
mongoose.connect(mongoURI,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
},function(err,res){
  if(err) console.log("Error connecting" + err);
  else console.log("Mongo Connected");
})
var bookingSchema = new schema({
  userId: {
        type: String,
        required: true
    },
  bookedBy: {
      type: String,
      required: true
  },
  slots: {
      type: Array
  },
  description: {
      type: String
  }
});

module.exports=mongoose.model('booked-slots', bookingSchema);