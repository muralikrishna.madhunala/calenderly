var mongoose=require("mongoose");
const dotenv = require("dotenv")
dotenv.config({path: "./config.env"})
var mongoURI=process.env.LOCAL_MONGO;
var schema=mongoose.Schema;
mongoose.connect(mongoURI,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
},function(err,res){
  if(err) console.log("Error connecting" + err);
  else console.log("Mongo Connected");
})
var loginSchema=new schema({
  emailId: {
      type: String,
      required: true,
      unique: true
    },
  password: {
      type: String,
      required: true
  },
  userId: {
      type: String,
      required: true
  }
});

module.exports=mongoose.model('logins', loginSchema);