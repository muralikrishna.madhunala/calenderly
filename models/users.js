var mongoose=require("mongoose");
const dotenv = require("dotenv")
dotenv.config({path: "./config.env"})
var mongoURI=process.env.LOCAL_MONGO;
var schema=mongoose.Schema;
mongoose.connect(mongoURI,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
},function(err,res){
  if(err) console.log("Error connecting" + err);
  else console.log("Mongo Connected");
})
var userSchema=new schema({
  name: {
      type: String,
      required: true
    },
  emailId: {
      type: String,
      required: true,
      unique: true
  },
  phoneNo: {
      type: Number,
      required: true
  }
});

module.exports=mongoose.model('users', userSchema);