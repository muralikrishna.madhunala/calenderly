const jwt = require('jsonwebtoken')
const dotenv = require("dotenv")
const userModel = require("../models/users")
const loginModel = require("../models/login")
const userSlotsModel = require('../models/user-slots')
const bookedSlotsModel = require('../models/booked-slots')

dotenv.config({path: "./config.env"})

exports.addUser = async function addUser(data){
    const userInfo = await userModel.findOne({emailId: data.emailId})
    if(userInfo){
        return {
            success: false,
            statusCode: 500,
            message: "User already exists"
        }
    }
    new userModel({
        name: data.name,
        emailId: data.emailId,
        phoneNo: data.phoneNo
    }).save((err, dbData, count)=>{
        
        new loginModel({
            emailId: dbData.emailId,
            password: data.password,
            userId: dbData._id
        }).save((err)=>{
            console.log("User added successfully")
        })
    })
    return {
        success:true,
        statusCode: 200,
        message: "User Added successfully"
    }
}

exports.getUser = async function(userId){
    const userInfo = await userModel.findOne({_id: userId})
    if(userInfo.length>0) return {
        data: userInfo,
        success: true,
        message: "Successfully executed"
    }
    return {
        success: false,
        message: "User not found"
    }
}

exports.getAllUsers = async function(){
    const usersInfo = await userModel.find();
    return {
        data: usersInfo,
        success: true,
        message: "Successfully executed"
    }
}

exports.updateUser = async function(userId, data){
    console.log("name: ", data.name, userId)
    const userData = await userModel.findOne({_id: userId});
    if(!userData){
        return {
            success: false,
            statusCode: 500,
            message: "User not found"
        }
    }
    for(key in data){
        console.log("Key: ", key, data[key])
        userData[key] = data[key]
    }
    userData.save()
    return {
        success: true,
        statusCode: 200,
        message: "User details updated successfully"
    }
}

exports.deleteUser = async function(userId){
    await userModel.deleteOne({_id: userId})
    return {
        success: true,
        message: "User deleted successfully",
        statusCode: 200
    }
}

exports.loginCheck = async function(data){
    let resData = {}
    const loginInfo = await loginModel.find({"emailId": data.emailId, "password": data.password})
    if(loginInfo.length == 0){
        resData.statusCode = 500
        resData.success = false;
        resData.message = "Please provide valid credentials"
    }
    else{
        const userId = loginInfo[0]._id;
        let token = jwt.sign({"userId": userId}, 
                process.env.JWT_SECRET,
                {
                    expiresIn: '1h'
                })
        resData.userId = userId;
        resData.token = token;
        resData.statusCode = 200;
        resData.success = true;
        resData.message = "User successfully logged in";   
    }
    return resData
}

exports.addUserSlots = async function(userId, data){
    const slotsData = await userSlotsModel.find({userId: userId});
    if(slotsData.length == 0){
        slotsData.slots = data.slots;
        slotsData.userId = userId;
        slotsData.save()
        return {
            success: true,
            statusCode: 200,
            message: "Slots added successfully"
        }
    }else{
        return {
            success: false,
            statusCode: 500,
            message: "Slots already available"
        }
    }
}

exports.updateUserSlots = async function(userId, data){
    const slotsData = await userSlotsModel.find({userId: userId});
    slotsData.slots = data.slots;
    slotsData.userId = userId;
    slotsData.save()

    const bookedSlotsData = await bookedSlotsModel.find({userId: userId});
    for(let i=0; i<bookedSlotsData.length; i++){
        const slots = bookedSlots.slots;
        for(let j=0; j<slots.length; j++){
            if(data.slots.indexOf(slots[i]) == -1){
                await bookedSlotsModel.deleteOne({_id: slots[i]._id})
            }
        }
    }
    return {
        success: true,
        statusCode: 200,
        message: "Slots updated successfully"
    }
}

exports.bookSlots = async function(userId, data){
    const bookedBy = data.userId;
    const bookedSlotsInfo = await bookedSlotsModel.find({userId: userId});
    let bookedSlots = [];
    for(let i=0; i<bookedSlotsInfo.length;i++){
        bookedSlots = bookedSlots.push(bookedSlotsInfo[i].slot);
    }
    const userSlots = await userSlotsModel.find({userId: userId})
    if(userSlots.slots.indexOf(data.slot) == -1){
        return {
            message: "Slot not available",
            statusCode: 500,
            success: false
        }
    }
    if(bookedSlots.indexOf(data.slot) == -1){
        new bookedSlotsModel({
            bookedBy: bookedBy,
            userId: userId,
            slot: data.slot,
            description: data.description
        }).save()
        return {
            message: "Slot booked successfully",
            statusCode: 200,
            success: true
        }
    }else{
        return {
            message: "Slot not available",
            statusCode: 500,
            success: false
        }
    }
}

