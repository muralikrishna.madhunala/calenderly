const dotenv = require("dotenv")
const express = require("express")
// const path = require("path")
const bodyparser = require("body-parser")
const index = require("./src/index")
const jwt = require("jsonwebtoken")

dotenv.config({path: "./config.env"})
const app = express()
const port = process.env.PORT || "8000"

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({ extended: false }));
app.listen(port, ()=>{
    console.log(`Listening on port ${port}`)
})

/**
 * 
 * @param req 
 * @param res 
 * @param next 
 * Used for authorizing the requests
 */
function authorization(req, res, next){

    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if(!token){
        return res.status(403).send({
            success: false,
            message: "Token not provided"
        })
    }
    else if(token.startsWith("Bearer ")){
        token = token.slice(7, token.length)
        if(token === ''){
            return res.status(403).send({
                success: false,
                message: "Token not provided"
            })
        }
    }
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded)=>{
        if(err){
            return res.status(403).send({
                success: false,
                message: "Token not valid"
            })
        }
        req.body.userId = decoded.userId;
        next()
    })
}
/**
 * input: {
 *     "emailId": "satvik@gmail.com",
 *     "password": "password.123"
 * }
 */
app.post('/login', async (req, res)=>{
    const resData = await index.loginCheck(req.body)
    res.status(resData.statusCode).send({
        success: resData.success,
        message: resData.message,
        userId: resData.userId,
        token: resData.token
    })
})

/**
 * Adds a new user
 * input body: {
 *     "name": "Satvik",
 *     "emailId": "satvik@gmail.com",
 *     "phoneNo": "9876543210"
 *     "password": "password.123"
 * }
 */
app.post('/users', authorization, async (req, res)=>{
    // console.log("Helloo: ", req.body);
    await index.addUser(req.body)
    res.send({"message":"User added successfully"})
})

/**
 * Gets the details of all the users
 */
app.get('/users', authorization, async (req, res)=>{
    console.log("Get all users")
    const resData = await index.getAllUsers()
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message,
        "data": resData.data
    })
})

/**
 * Gets the details of the specific user
 */
app.get('/users/:userId', authorization, async (req, res)=>{
    console.log("Hello")
    const resData = await index.getUser(req.params.userId)
    res.send(resData)
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message,
        "data": resData.data
    })
})

/**
 * Updates the data for a particular user
 */
app.put('/users/:userId', authorization, async (req, res)=>{
    console.log("req data: ", req.body, req.params.userId);
    await index.updateUser(req.params.userId, req.body)
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message
    })
})

/**
 * Deletes the specific user
 */
app.delete('/users/:userId', authorization, async (req, res)=>{
    await index.deleteUser(req.params.userId)
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message
    })
})

/**
 * Inserts the user availability slots
 * path parameter: userId
 * input body: {
 *     "userId": "fiauG9139RGQEHFW0SA",
 *     "slots": ["10:00-11:00", "12:00-13:00" ... ]
 * }
 */
app.post('/users/:userId/slots', authorization, async (req, res)=>{
    const resData = await index.addUserSlots(req.params.userId, req.body.slots)
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message
    })
})

/**
 * path parameter: userId
 * input body: {
 *     "userId": "fiauG9139RGQEHFW0SA",
 *     "slots": ["10:00-11:00", "12:00-13:00" ... ]
 * }
 */
app.put('/users/:userId/slots', authorization, async (req, res)=>{
    const resData = await index.updateUserSlots(req.params.userId, req.body.slots)
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message
    })
})

/**
 * path parameter: userId - The user for which you want to book a a slot with
 * input body: {
 *      "slot": "12:00-13:00",
 *      "description": "To have a scrum meeting"
 * }
 */
app.post('/bookSlot/:userId', authorization, async (req, res)=>{
    const resData = await index.bookSlot(req.params.userId, req.body)
    res.status(resData.statusCode).send({
        "success": resData.success,
        "message": resData.message
    })
})
